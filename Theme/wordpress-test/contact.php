<?php
/* 
template name:Contact
*/
 get_header();?>
<div id="container" class="homepage">
	<header>
		<?php 
			get_template_part( 'nav' ); 
			get_template_part( 'banners' ); 
		?>
	</header>
	<?php if(have_posts()) : while(have_posts()) : the_post();?>
	<article>
		<div class="shadow">
			<div class="contentholder">
				<div class="shadowcontentholder clearfix">
					<div class="content">
						<div class="contactholder">
							<div class="map">
								<a href="https://www.google.com/maps/preview?f=q&source=newuser-ws&hl=en&geocode=&q=330+Madison+Avenue+New+York,+NY+10017&aq=&sll=33.090505,-96.842637&sspn=0.00738,0.013937&ie=UTF8&hq=&hnear=330+Madison+Ave,+New+York,+10017&z=17" target="_blank">
									<img src="http://127.0.0.1/wordpress/wp-content/uploads/map.png" alt="map" width="320" height="183" />
								</a>
							</div>
							<?php the_content();?>
							<div class="contactform">
								<p>We would to hear from you!  Feel free to contact us at any time regarding <br /> any questions or concerns you may have.  <span class="required">*</span> <span style="color:white">= required</span></p>
								<?php echo do_shortcode('[easy_contact_forms fid=1]');?>
							</div>
						</div>
					</div>
					<div class="sidebar">
						<?php get_sidebar();?>
					</div>
				</div>
			</div>
		</div>
	</article>
	<?php endwhile; endif;?>
<?php get_footer();?>