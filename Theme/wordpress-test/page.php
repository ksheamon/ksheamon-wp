<?php get_header();?>
<div id="container">
	<header>
		<?php 
			get_template_part('nav');
			get_template_part('banners');
		?>
	</header>
	<?php 
		if(have_posts()) : while(have_posts()) : the_post();
	?>
	<article>
		<div class="shadow">
			<div class="contentholder">
				<div class="shadowcontentholder clearfix">
					<div class="content">
						<?php get_template_part('contact-side');?>
						<div class="copy">
							<?php the_content();?>
						</div>
						<?php
							if(is_page('contact-us')){
								get_template_part( 'contact-form' );
							}
						?>
					</div>
					<div class="sidebar">
						<?php get_sidebar();?>
					</div>
				</div>
			</div>
		</div>
	</article>
	<?php endwhile; endif;?>
<?php get_footer();?>