<!-- sidebar -->
<?php
  if($post->post_parent)
	$children = wp_list_pages("title_li=&child_of=".$post->post_parent."&echo=0");
  else
	$children = wp_list_pages("title_li=&child_of=".$post->ID."&echo=0");
  if ($children) {
?>

 <!-- Sub Menu -->
<div id="submenu">
	<ul>
		<?php echo $children; ?>
	</ul>
 </div> <!-- / Sub Menu -->

 <?php
	} 
?>
<!-- /sidebar -->
