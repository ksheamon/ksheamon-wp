<?php 
/*
template name: Services
*/

get_header();?>
<div id="container">
	<header>
	<?php 
		get_template_part('nav');
		get_template_part('banners');
	?>
	</header>
	<article>
		<div class="shadow">
			<div class="contentholder">
				<div class="shadowcontentholder clearfix">
					<div class="content">
						<?php get_template_part('contact-side');?>
						<div class="ohidden">
							<ul class="services">
							<?php
								$i=1;					
								$new = new WP_Query(array('post_type' => 'service', 'orderby' => 'date', 'order' => 'ASC'));
									while ($new->have_posts()) : $new->the_post(); 
							?>
								<li>
									<h2 class="title clearfix">
										<?php
											the_post_thumbnail();
											the_title();
										?>										
									</h2>
									<div class="desc"><?php the_excerpt();?></div>
									<div class="btnsection clearfix"><a href="#" onclick="showBox(<?php echo $i?>)" class="btn moresmall link">more</a></div>
								</li>
								<div class="box box<?php echo $i?>">
									<span class="close"><a href="#" onclick="hideBox()">X</a></span>
									<h2 class="title greytitle clearfix">
										<?php
											the_post_thumbnail();
											the_title();
										?>										
									</h2>
									<div>
										<?php the_content();?>
									</div>
								</div>
							<?php 
								$i++;
								endwhile;
							?>
							</ul>
						</div>
					</div>
					<div class="sidebar">
						<?php
							wp_reset_query(); 
							get_sidebar();
						?>
					</div>
				</div>
			</div>
		</div>
	</article>
	<div class="black" onclick="hideBox()"></div>
<?php get_footer();?>