<?php 
/*
template name: FAQs
*/
if(have_posts()): while(have_posts()): the_post();
	get_header();?>
	<div id="container">
		<header>
			<?php 
				get_template_part('nav');
				get_template_part('banners');
			?>
		</header>
		<article>
			<div class="shadow">
				<div class="contentholder">
					<div class="shadowcontentholder clearfix">
						<div class="content">
							<?php get_template_part('contact-side');?>
							<div class="copy">
								<?php the_content();?>
								<div class="qa">
									<?php 
										$i=1;
										$new = new WP_Query(array('post_type' => 'faq', 'orderby' => 'date', 'order' => 'ASC'));
										while ($new->have_posts()) : $new->the_post();?>
											<a href="#" onclick="showAnswer(<?php echo $i?>)" class="title question question<?php echo $i?>">
												<h2>
													<span><?php the_title();?></span>
												</h2>
											</a>
											<div class="answer answer<?php echo $i?>">
												<?php the_content();?>
											</div>					
									<?php 
										$i++;
										endwhile;
									?>
								</div>
							</div>
						</div>
						<div class="sidebar">
							<?php get_sidebar();?>
						</div>
					</div>
				</div>
			</div>
		</article>
<?php endwhile; endif;?>
<?php get_footer();?>