<?php get_header();?>
<div id="container" class="homepage">
	<header>
		<?php 
			get_template_part( 'nav' );
			get_template_part('banners');
		?>
		
	</header>
	<article>
		<div class="shadow">
			<div class="homecontent">
				<div class="homeboxes clearfix">
					<?php
						$n=1;
						$new = new WP_Query(array('post_type' => 'home-cta', 'orderby' => 'date', 'order' => 'ASC'));
						while ($new->have_posts()) : $new->the_post();?>
							<div class="homebox">
								<div class="imgsection">
									<div class="cta<?php echo $n;?>"><?php the_post_thumbnail();?></div>					
								</div>
								<h2 class="title"><?php the_title();?></h2>
								<div class="desc"><?php the_excerpt();?></div>
								<ul>
									<?php 
										for($i=1; $i<=4; $i++){
											$query= get_post_meta(get_the_ID(), 'cta_list'.$i);
											foreach ($query as $key => $value){
												echo '<li><span>'.$value.'</span></li>';
											}						
										}
									?>
								</ul>
								<?php
									wp_reset_query();
									switch($n){
										case 1:
											echo '<a href="'.home_url().'/services" class="btn services">ALL OUR SERVICES</a>';
											break;
										case 2:
											echo '<a href="'.home_url().'/fund-management" class="btn more">LEARN MORE</a>';
											break;
										case 3:
											echo '<a href="'.home_url().'/contact" class="btn contact">CONTACT US</a>';
											break;
									}	
								?>		
							</div>
						<?php 
							$n++; 
							endwhile;
						?>	
				</div>
			</div>
		</div>
	</article>
<?php get_footer();?>