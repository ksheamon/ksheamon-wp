<?php
/*
template name: Sitemap
*/
get_header();?>
<div id="container">
	<header>
		<?php 
			get_template_part( 'nav' );
			get_template_part( 'banners' ); 
		?>
	</header>
	<?php
		if(have_posts()) : while(have_posts()) : the_post();
	?>
	<article>
		<div class="shadow">
			<div class="contentholder">
				<div class="shadowcontentholder clearfix">
					<div class="content">
						<div class="pages">
							<ul class="sitelist">
								<?php wp_list_pages('title_li='); ?>
							</ul>	
						</div>					
					</div>
					<div class="sidebar">
						<?php get_sidebar();?>
					</div>
				</div>
			</div>
		</div>
	</article>
	<?php 
		endwhile; endif;
	?>
<?php get_footer();?>
