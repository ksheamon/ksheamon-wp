<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="description" content="">
	<meta name="keywords" content="" />
	<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;">
	<link rel="shortcut icon" href="/favicon.ico">
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri();?>/style.css">


<title><?php wp_title( '|', true, 'right' ); echo get_bloginfo('name');?></title>
<?php wp_head();?>
</head>