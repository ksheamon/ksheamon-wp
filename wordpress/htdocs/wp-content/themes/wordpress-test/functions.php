<?php

if (function_exists('add_theme_support'))
{
	// Add Menu Support
    add_theme_support('menus');
	
	//Add Post Thumbnails
	add_theme_support( 'post-thumbnails' ); 
}

// Remove Admin bar
function remove_admin_bar()
{
    return false;
}


/*--- REGISTER CUSTOM POST TYPE FOR HOME PAGE CTAS---*/

function cta_post_type() {

	$labels = array(
		'name'                => _x( 'home-ctas', 'Post Type General Name', 'text_domain' ),
		'singular_name'       => _x( 'home-cta', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'           => __( 'Home CTAs', 'text_domain' ),
		'parent_item_colon'   => __( 'Parent:', 'text_domain' ),
		'all_items'           => __( 'All', 'text_domain' ),
		'view_item'           => __( 'View', 'text_domain' ),
		'add_new_item'        => __( 'Add New', 'text_domain' ),
		'add_new'             => __( 'New', 'text_domain' ),
		'edit_item'           => __( 'Edit', 'text_domain' ),
		'update_item'         => __( 'Update', 'text_domain' ),
		'search_items'        => __( 'Search items', 'text_domain' ),
		'not_found'           => __( 'No items found', 'text_domain' ),
		'not_found_in_trash'  => __( 'No items found in Trash', 'text_domain' ),
	);
	$args = array(
		'label'               => __( 'home-cta', 'text_domain' ),
		'description'         => __( 'Home Page CTAs', 'text_domain' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'excerpt', 'custom-fields', 'thumbnail' ),
		'taxonomies'          => array( 'category', 'post_tag' ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 5,
		'menu_icon'           => 'http://127.0.0.1/wordpress/wp-content/themes/wordpress-test/images/hand.png',
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
	);
	register_post_type( 'home-cta', $args );

}
// Hook into the 'init' action
add_action( 'init', 'cta_post_type', 0 );




/*--- REGISTER CUSTOM POST TYPE FOR FAQS---*/


function faq_post_type() {

	$labels = array(
		'name'                => _x( 'faqs', 'Post Type General Name', 'text_domain' ),
		'singular_name'       => _x( 'faq', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'           => __( 'FAQs', 'text_domain' ),
		'parent_item_colon'   => __( 'Parent:', 'text_domain' ),
		'all_items'           => __( 'All', 'text_domain' ),
		'view_item'           => __( 'View', 'text_domain' ),
		'add_new_item'        => __( 'Add New', 'text_domain' ),
		'add_new'             => __( 'New', 'text_domain' ),
		'edit_item'           => __( 'Edit', 'text_domain' ),
		'update_item'         => __( 'Update', 'text_domain' ),
		'search_items'        => __( 'Search items', 'text_domain' ),
		'not_found'           => __( 'No items found', 'text_domain' ),
		'not_found_in_trash'  => __( 'No items found in Trash', 'text_domain' ),
	);
	$args = array(
		'label'               => __( 'faq', 'text_domain' ),
		'description'         => __( 'faqs', 'text_domain' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', ),
		'taxonomies'          => array( 'category', 'post_tag' ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 5,
		'menu_icon'           => 'http://127.0.0.1/wordpress/wp-content/themes/wordpress-test/images/question.png',
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
	);
	register_post_type( 'faq', $args );

}
// Hook into the 'init' action
add_action( 'init', 'faq_post_type', 0 );



/*--- REGISTER CUSTOM POST TYPE FOR ASSOCIATES---*/


function associate_post_type() {

	$labels = array(
		'name'                => _x( 'associates', 'Post Type General Name', 'text_domain' ),
		'singular_name'       => _x( 'associate', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'           => __( 'Associates', 'text_domain' ),
		'parent_item_colon'   => __( 'Parent:', 'text_domain' ),
		'all_items'           => __( 'All', 'text_domain' ),
		'view_item'           => __( 'View', 'text_domain' ),
		'add_new_item'        => __( 'Add New', 'text_domain' ),
		'add_new'             => __( 'New', 'text_domain' ),
		'edit_item'           => __( 'Edit', 'text_domain' ),
		'update_item'         => __( 'Update', 'text_domain' ),
		'search_items'        => __( 'Search items', 'text_domain' ),
		'not_found'           => __( 'No items found', 'text_domain' ),
		'not_found_in_trash'  => __( 'No items found in Trash', 'text_domain' ),
	);
	$args = array(
		'label'               => __( 'associate', 'text_domain' ),
		'description'         => __( 'associates', 'text_domain' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'excerpt', 'thumbnail' ),
		'taxonomies'          => array( 'category', 'post_tag' ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 5,
		'menu_icon'           => 'http://127.0.0.1/wordpress/wp-content/themes/wordpress-test/images/people.png',
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
	);
	register_post_type( 'associate', $args );

}
// Hook into the 'init' action
add_action( 'init', 'associate_post_type', 0 );



/*--- REGISTER CUSTOM POST TYPE FOR SERVICES---*/

function service_post_type() {

	$labels = array(
		'name'                => _x( 'services', 'Post Type General Name', 'text_domain' ),
		'singular_name'       => _x( 'service', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'           => __( 'Services', 'text_domain' ),
		'parent_item_colon'   => __( 'Parent:', 'text_domain' ),
		'all_items'           => __( 'All Services', 'text_domain' ),
		'view_item'           => __( 'View Services', 'text_domain' ),
		'add_new_item'        => __( 'Add New Service', 'text_domain' ),
		'add_new'             => __( 'New Service', 'text_domain' ),
		'edit_item'           => __( 'Edit Service', 'text_domain' ),
		'update_item'         => __( 'Update Service', 'text_domain' ),
		'search_items'        => __( 'Search Services', 'text_domain' ),
		'not_found'           => __( 'No Services Found', 'text_domain' ),
		'not_found_in_trash'  => __( 'No Services Found in Trash', 'text_domain' ),
	);
	$args = array(
		'label'               => __( 'service', 'text_domain' ),
		'description'         => __( 'Services', 'text_domain' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'excerpt', 'thumbnail', ),
		'taxonomies'          => array( 'category', 'post_tag' ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 5,
		'menu_icon'           => 'http://127.0.0.1/wordpress/wp-content/themes/wordpress-test/images/gear.png',
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
	);
	register_post_type( 'service', $args );

}

// Hook into the 'init' action
add_action( 'init', 'service_post_type', 0 );




/*--- CREATE BREADCRUMB NAVIGATION---*/

function make_breadcrumbs(){
 	global $post;
    	echo '<ul class="breadcrumbs"><li><a href="'.site_url().'">Home</a></li>';
	 if($post->post_parent){
                $anc = get_post_ancestors( $post->ID );
                $title = get_the_title();
                foreach ( $anc as $ancestor ) {
                    $output = '<li><a href="'.get_permalink($ancestor).'" title="'.get_the_title($ancestor).'">'.get_the_title($ancestor).'</a></li>';
                }
                echo $output;
                echo '<li><strong title="'.$title.'"> '.$title.'</strong></li>';
            } else {
                echo '<li><strong> '.get_the_title().'</strong></li>';
            }
	echo '</ul>';
}



/*--- GENERATE SITEMAP FOR XML DOC---*/

add_action("publish_post", "eg_create_sitemap");
add_action("publish_page", "eg_create_sitemap");

function create_sitemap() {
  $posts = get_posts(array(
    'numberposts' => -1,
    'orderby' => 'modified',
    'post_type'  => array('post','page'),
    'order'    => 'DESC'
  ));

  $sitemap = '<?xml version="1.0" encoding="UTF-8"?>';
  $sitemap .= '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';

  foreach($posts as $post) {
    setup_postdata($post);

    $postdate = explode(" ", $post->post_modified);

    $sitemap .= '<url>'.
      '<loc>'. get_permalink($post->ID) .'</loc>'.
      '<lastmod>'. $postdate[0] .'</lastmod>'.
      '<changefreq>monthly</changefreq>'.
    '</url>';
  }

  $sitemap .= '</urlset>';

  $fp = fopen(ABSPATH . "sitemap.xml", 'w');
  fwrite($fp, $sitemap);
  fclose($fp);
}


/*--- ADD EXTRA META BOXES FOR HOME CTAS---*/

// Add the Meta Box
function add_custom_meta_box() {
    add_meta_box(
		'custom_meta_box', // $id
		'CTA List Items', // $title
		'show_custom_meta_box', // $callback
		'home-cta', // $page
		'normal', // $context
		'high'); // $priority
}
add_action('add_meta_boxes', 'add_custom_meta_box');

// Field Array
$prefix = 'cta_';
$custom_meta_fields = array(
	array(
		'label'	=> 'List Item 1',
		'desc'	=> ' ',
		'id'	=> $prefix.'list1',
		'type'	=> 'text'
	),
	array(
		'label'	=> 'List Item 2',
		'desc'	=> ' ',
		'id'	=> $prefix.'list2',
		'type'	=> 'text'
	),
	array(
		'label'	=> 'List Item 3',
		'desc'	=> ' ',
		'id'	=> $prefix.'list3',
		'type'	=> 'text'
	),
	array(
		'label'	=> 'List Item 4',
		'desc'	=> ' ',
		'id'	=> $prefix.'list4',
		'type'	=> 'text'
	)
);


// add some custom js to the head of the page
add_action('admin_head','add_custom_scripts');
function add_custom_scripts() {
	global $custom_meta_fields, $post;

	$output = '<script type="text/javascript">
				jQuery(function() {';

	foreach ($custom_meta_fields as $field) { // loop through the fields looking for certain types
		// date
		if($field['type'] == 'date')
			$output .= 'jQuery(".datepicker").datepicker();';
		// slider
		if ($field['type'] == 'slider') {
			$value = get_post_meta($post->ID, $field['id'], true);
			if ($value == '') $value = $field['min'];
			$output .= '
					jQuery( "#'.$field['id'].'-slider" ).slider({
						value: '.$value.',
						min: '.$field['min'].',
						max: '.$field['max'].',
						step: '.$field['step'].',
						slide: function( event, ui ) {
							jQuery( "#'.$field['id'].'" ).val( ui.value );
						}
					});';
		}
	}

	$output .= '});
		</script>';

	echo $output;
}

// The Callback
function show_custom_meta_box() {
	global $custom_meta_fields, $post;
	// Use nonce for verification
	echo '<input type="hidden" name="custom_meta_box_nonce" value="'.wp_create_nonce(basename(__FILE__)).'" />';

	// Begin the field table and loop
	echo '<table class="form-table">';
	foreach ($custom_meta_fields as $field) {
		// get value of this field if it exists for this post
		$meta = get_post_meta($post->ID, $field['id'], true);
		// begin a table row with
		echo '<tr>
				<th><label for="'.$field['id'].'">'.$field['label'].'</label></th>
				<td><input type="text" name="'.$field['id'].'" id="'.$field['id'].'" value="'.$meta.'" size="59" />
								<br /><span class="description">'.$field['desc'].'</span>
				</td></tr>';
	} // end foreach
	echo '</table>'; // end table
}

function remove_taxonomy_boxes() {
	remove_meta_box('categorydiv', 'post', 'side');
}
add_action( 'admin_menu' , 'remove_taxonomy_boxes' );

// Save the Data
function save_custom_meta($post_id) {
    global $custom_meta_fields;

	// verify nonce
	if (!wp_verify_nonce($_POST['custom_meta_box_nonce'], basename(__FILE__)))
		return $post_id;
	// check autosave
	if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)
		return $post_id;
	// check permissions
	if ('page' == $_POST['post_type']) {
		if (!current_user_can('edit_page', $post_id))
			return $post_id;
		} elseif (!current_user_can('edit_post', $post_id)) {
			return $post_id;
	}

	// loop through fields and save the data
	foreach ($custom_meta_fields as $field) {
		if($field['type'] == 'tax_select') continue;
		$old = get_post_meta($post_id, $field['id'], true);
		$new = $_POST[$field['id']];
		if ($new && $new != $old) {
			update_post_meta($post_id, $field['id'], $new);
		} elseif ('' == $new && $old) {
			delete_post_meta($post_id, $field['id'], $old);
		}
	} // enf foreach

	// save taxonomies
	$post = get_post($post_id);
	$category = $_POST['category'];
	wp_set_object_terms( $post_id, $category, 'category' );
}
add_action('save_post', 'save_custom_meta');


/*---HIGHLIGHT CURRENT PAGE / ANCESTOR PAGE ON MAIN NAV---*/
function highlight_menu($pagename){
	wp_reset_query();
	
	if (is_page($pagename)){  //current page
		echo ' class="current_page_item"'; 
	}	
	else{ //query parent items
		global $post;
       		$parents = get_post_ancestors( $post->ID );
       		$id = ($parents) ? $parents[count($parents)-1]: $post->ID;
	        $parent = get_page( $id );
		$parentname= $parent->post_name;
		if ($parentname == $pagename){
			echo ' class="current_page_ancestor"'; 
		}
	}
}











?>