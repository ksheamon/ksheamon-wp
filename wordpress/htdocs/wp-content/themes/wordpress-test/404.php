<?php get_header();?>
<div id="container">
	<header>
		<?php get_template_part( 'nav' ); ?>
		<?php get_template_part( 'banners' ); ?>
		
	</header>
	<article>
		<div class="shadow">
			<div class="contentholder">
				<div class="shadowcontentholder clearfix">
					<div class="content">
						<div class="content">
							<div class="pages error">
								<h2 class="title">Page Not Found</h2>
								<ul class="sitelist">
									<?php wp_list_pages('title_li='); ?>
								</ul>	
							</div>					
						</div>				
							
					</div>
				</div>
			</div>
		</div>
	</article>
	<?php get_footer();?>