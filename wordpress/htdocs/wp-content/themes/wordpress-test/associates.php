<?php 
/*
template name: Associates
*/

get_header();?>
<div id="container">
	<header>
	<?php 
		get_template_part('nav');
		get_template_part('banners');
	?>
	</header>
	<article>
		<div class="shadow">
			<div class="contentholder">
				<div class="shadowcontentholder clearfix">
					<div class="content">
							<?php get_template_part('contact-side');?>
						<div class="copy">
							<div>
								<?php 
									$i=1;					
									$new = new WP_Query(array('post_type' => 'associate', 'orderby' => 'date', 'order' => 'ASC'));
									while ($new->have_posts()) : $new->the_post(); 
										$default_attr = array(
										'class'	=> " fleft padright",
									);
									the_post_thumbnail($default_attr);
								?>
								<h2 class="associatename">
									<?php the_title();?>
								</h2>
								<div class="desc">
									<?php the_excerpt();?>
								</div>
								<br style="clear:both;"/>
							</div>
							<?php 
								$i++;
								endwhile;
							?>
						</div>
						<div class="sidebar">
							<?php
								wp_reset_query();
								get_sidebar();
							?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</article>
<?php get_footer();?>