<?php 
	if(is_home()){
?>
<div class="mainbanner">
			<div class="shadow">
				<div class="bannerholder clearfix">
					<img src="<?php echo get_template_directory_uri();?>/images/mainbanner.jpg" alt="" />
					<div class="textsection">
						<div>
							<div class="text">Serving Customers Since 2002</div>
								<h1 class="title"><strong>Lifetrust</strong> <span>strives to build <br /> long term relationships and</span><br /> considers each investor as a partner.</h1>
								<div class="clearfix">
									<a href="why-us/invest" class="btn invest">Invest as a partner</a>
									<a href="contact" class="btn contactbig">Contact Us</a>
								</div>
						</div>
					</div>
				</div>
			</div>
		</div>
<?php } else { ?>

<div class="mainbanner">
	<div class="shadow">
		<div class="bannerholder clearfix">
			<?php 
				if ($post->post_parent){    //check for parent post
					if (has_post_thumbnail($post->post_parent, array (360,114), 'pagethumb') ) {     //check for featured image on parent post for banner
						echo get_the_post_thumbnail($post->post_parent, 'pagethumb');
           	 		} else {
						echo '<img src="' . get_template_directory_uri() . '/images/contact.jpg" alt="" width="360" height="114"/>';
					}
				} elseif(has_post_thumbnail()){    //if page has not parent, check for page image
					echo get_the_post_thumbnail(get_the_id());
				}else echo '<img src="' . get_template_directory_uri() . '/images/contact.jpg" alt="" width="360" height="114"/>';    //default if no featured image set
			?>
			<div class="textsection">
				<div class="lefttext">
					<?php if(is_404()){?>
						<h1 class="pagetitle">404 Error</h1>
					<?php } else {?>
						<h1 class="pagetitle"><?php the_title();?></h1>
					<?php } make_breadcrumbs();?>
				</div>
				<div class="righttext">
					<blockquote>
						<div class="lalign">“our core value is that some mission statement</div>
						<div class="ralign">or testimonial goes here in this area...”</div>
					</blockquote>
				</div>
			</div>
		</div>
	</div>
</div>
<?php } ?>