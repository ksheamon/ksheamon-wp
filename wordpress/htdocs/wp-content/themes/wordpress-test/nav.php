<nav class="supernav">
	<div class="frame clearfix">
		<ul>
			<li><a href="<?php echo site_url();?>/industry-overview">Industry Overview</a></li>
			<li><a href="<?php echo site_url();?>/sitepages">Sitemap</a></li>
		</ul>
	</div>
</nav>
<nav class="mainnav">
	<div class="frame clearfix">
		<strong class="logo"><a href="<?php echo site_url();?>"><img src="<?php echo get_template_directory_uri();?>/images/logo.png" alt="Lifetrust" /></a></strong>
		<ul>
			<li <?php highlight_menu('about-us'); ?>><a href="<?php echo site_url();?>/about-us">About</a></li>
			<li <?php highlight_menu('services'); ?>><a href="<?php echo site_url();?>/services">Services</a></li>
			<li <?php highlight_menu('why-us'); ?>><a href="<?php echo site_url();?>/why-us">Why Us</a></li>
			<li <?php highlight_menu('faqs'); ?>><a href="<?php echo site_url();?>/faqs">Faq</a></li>
			<li <?php highlight_menu('contact'); ?>><a href="<?php echo site_url();?>/contact">Contact</a></li>
		</ul>
	</div>
</nav>