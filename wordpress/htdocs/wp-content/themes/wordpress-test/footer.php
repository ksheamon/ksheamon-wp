<footer>
		<div class="frame">
			<div class="row clearfix">
				<ul>
					<li><a href="<?php echo site_url();?>/about-us">About</a></li>
					<li><a href="<?php echo site_url();?>/services">Services</a></li>
					<li><a href="<?php echo site_url();?>/why-us">Why Us</a></li>
					<li><a href="<?php echo site_url();?>/faqs">FAQ</a></li>
					<li><a href="<?php echo site_url();?>/contact">Contact</a></li>
				</ul>
				<div class="privacy"><a href="<?php echo site_url();?>/privacy-policy">Privacy Policy</a></div>
			</div>
			<div class="row clearfix">
				<div class="copyright">&copy; <?php echo date('Y');?> LifeTrust. All Rights Reserved.</div>
				<div class="by"><a href="http://www.bluefountainmedia.com" target="_blank">Website Design</a> by <a href="http://www.bluefountainmedia.com/blog" target="_blank">Blue Fountain Media</a></div>
			</div>
		</div>
</footer>
</div><!--CLOSE CONTAINER-->
<script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
<script src="<?php echo get_template_directory_uri();?>/js/java.js"></script>
<?php wp_footer();?>
</body>
</html>